import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainPresenter {
    private Injector injector;
    private Stage stage;
    private iView view;
    private iPresenter presenter = null;

    public MainPresenter(Stage stage) throws IOException {
        this.stage = stage;
        init();
        ResponseOnActionEvent(null);
    }

    @Subscribe
    public void ResponseOnActionEvent(ActionEvent event) throws IOException {
        System.out.println("Subscriber");
        if(presenter instanceof Presenter2){
            this.presenter = injector.getInstance(Presenter.class);
            this.view = new View();
        } else {
                this.presenter = injector.getInstance(Presenter2.class);
                this.view = new View2();
        }
        FXMLLoader loader = view.bakeFXML();
        loader.setController(presenter);
        this.stage.setScene(new Scene((Parent) loader.load()));
        this.stage.show();
    }

    private boolean init(){
        try{
            EventBus bus = new EventBus();
            this.injector = Guice.createInjector(new InjectorModule(bus));
            NettyCommunication netty = injector.getInstance(NettyCommunication.class);

            bus.register(netty);
            bus.register(this);

            return true;
        } catch (Exception e){
            return false;
        }
    }


}
