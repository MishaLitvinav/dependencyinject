import com.google.common.eventbus.EventBus;
import javafx.application.Application;
import javafx.stage.Stage;

public class AppStarter extends Application {
    public static void main(String[] args){
        launch();
    }

    public void start(Stage stage) throws Exception {
        new MainPresenter(stage);
    }
}
