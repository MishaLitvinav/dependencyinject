import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import javafx.event.ActionEvent;

public class Presenter2 implements iPresenter{
    @Inject
    private EventBus bus;

    public void TestButton(ActionEvent event){
        System.out.println("clicked");
        bus.post(event);
    }
}