import javafx.fxml.FXMLLoader;

public class View2 implements iView{
    public FXMLLoader bakeFXML(){
        System.out.println("View #2");
        FXMLLoader loader = new FXMLLoader (getClass().getResource("/test2.fxml"));
        return loader;
    }
}
