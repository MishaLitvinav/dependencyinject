import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;

public class InjectorModule extends AbstractModule {
    private EventBus bus;

    public InjectorModule(EventBus bus) {
        this.bus = bus;
        registerAll(bus);
    }

    private void registerAll(EventBus bus) {
        bus.register(Presenter.class);
    }

    public void configure(){
        bind(EventBus.class).toInstance(bus);
    }
}